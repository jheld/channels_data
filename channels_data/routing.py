from channels.routing import ProtocolTypeRouter, URLRouter

import feed.routing

application = ProtocolTypeRouter({
    "websocket": URLRouter(feed.routing.websocket_urlpatterns)
}
)