from django.db import models
from django.conf import settings

# Create your models here.


class Item(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)


class Feed(models.Model):
    topic = models.CharField(max_length=254, db_index=True)
    items = models.ManyToManyField(Item)
    followers = models.ManyToManyField(settings.AUTH_USER_MODEL)



