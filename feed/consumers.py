from channels.generic.websocket import JsonWebsocketConsumer
from asgiref.sync import async_to_sync

from django.contrib.auth import get_user_model

from feed.models import Feed


class FeedConsumer(JsonWebsocketConsumer):
    user = None

    def connect(self):
        self.accept()
        self.user = get_user_model().objects.get(id=1)
        feeds = Feed.objects.filter(followers=self.user)
        for feed_id in feeds.all().values_list("id", flat=True):
            async_to_sync(self.channel_layer.group_add)(
                f"feed_{feed_id}",
                self.channel_name,
            )
            self.groups.append(f"feed_{feed_id}")

        self.send_json({"feeds": [item for item in self.groups if item.startswith("feed")]})

    def receive_json(self, content, **kwargs):
        pass

    def feed_update(self, content):
        self.send_json({"feed": content})
