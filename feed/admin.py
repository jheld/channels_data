from django.contrib import admin

from .models import Feed, Item

# Register your models here.
@admin.register(Feed)
class FeedAdmin(admin.ModelAdmin):
    pass

@admin.register(Item)
class ItemAdmin(admin.ModelAdmin):
    pass